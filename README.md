# shunt-tags

Script to rename filenames in a certain way.

`shunt_tags` shunt tags to the end of filenames, transforming `[RELEASEGROUP] Title - Episode [CRC32].mkv` into `Title - Episode [RELEASEGROUP CRC32].mkv`, in order to improve alphabetical sort ordering. The original goal has expanded into a large number of transforms with a comprehensive test suite.

## Usage

```
Usage:
  shunt_tags [OPTIONS]
  
Options:
  -y  --accept                  Apply updates without prompting. (false)
  -d  --directory DIR           Set directory. (pwd)
      --display table|dump|none Choose display mode. ('table')
  -n  --dry-run                 Display proposed updates and exit. (false)
      --help, --usage           Display this message
      --interactive             Prompt whether to apply updates. (true)
  -q  --quiet                   Quiet mode hides scanning progress. (false)
  -qq --quiet2                  Equivalent to "-q --display none"
  -r  --recursive               Apply to subdirectories. (false)
      --test-suite              Run test suite and exit  
      --undo                    Display an undo script
```

## Changelog

2018-09-30 v3.1.0
- Apply titlecasing for all-lowercase filenames
- Support quirks for `[a-s]` filenames
- Fix an issue with running `shunt-tags` from the current working directory

2017-10-22 v3.0
- Rewritten in PHP
- Feature: Many more features now available via command-line arguments

2017-04-05 v2.0
- Rewritten in bash, now requires linux ports of `rren` and `pause`
- Feature: Support `--graphical` argument to relaunch itself under `xfce4-terminal`

201?-??-?? v1.5
- Support additional filename transforms

2015-01-17 v1.0
- Initial public release
- Written in batch, requires `rren` in path
